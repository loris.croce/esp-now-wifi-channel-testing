#define END_NODE // uncomment for syntax check in pio
#ifdef END_NODE

#include <esp_now.h>
#include <WiFi.h>
#include <esp_wifi.h>

#include "secrets.h"

RTC_DATA_ATTR unsigned long msgId = 1;
bool previousReceived = false;

bool messageSent = false;

#define MAX_TRIES 5

// Deep sleep
#define DEEP_SLEEP_TIME 600
#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds */

// Peers (not using esp_now_peer_info because there's no "next")

typedef struct peer_t
{
    uint8_t address[ESP_NOW_ETH_ALEN];
    int channel;
    peer_t *next;
} peer_t;

peer_t bridge1;
peer_t bridge6;
peer_t bridge11;

RTC_DATA_ATTR peer_t *currentPeer = &bridge1; // starting at 1

void initPeers()
{
    memcpy(bridge1.address, ADDRESS_1, ESP_NOW_ETH_ALEN);
    memcpy(bridge6.address, ADDRESS_6, ESP_NOW_ETH_ALEN);
    memcpy(bridge11.address, ADDRESS_11, ESP_NOW_ETH_ALEN);
    bridge1.channel = 1;
    bridge6.channel = 6;
    bridge11.channel = 11;
    bridge1.next = &bridge6;
    bridge6.next = &bridge11;
    bridge11.next = &bridge1;
}

esp_now_peer_info_t bridge_node;

// ESP-NOW message struct
typedef struct message
{
    unsigned long id;
    float value;
    char type[16];
} message_t;

message_t message;

bool addPeer(peer_t node)
{
    // add pairing
    memset(&bridge_node, 0, sizeof(bridge_node));
    const esp_now_peer_info_t *peer = &bridge_node;
    memcpy(bridge_node.peer_addr, node.address, 6);

    bridge_node.channel = node.channel; // pick a channel
    bridge_node.encrypt = 0;            // no encryption
    // check if the peer exists
    bool exists = esp_now_is_peer_exist(bridge_node.peer_addr);
    if (exists)
    {
        // bridge_node already paired.
        Serial.println("Already Paired");
        return true;
    }
    else
    {
        esp_err_t addStatus = esp_now_add_peer(peer);
        if (addStatus == ESP_OK)
        {
            // Pair success
            Serial.println("Pair success");
            return true;
        }
        else
        {
            Serial.println("Pair failed");
            return false;
        }
    }
}

void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status)
{
    Serial.print(F("\r\nMaster packet sent:\t"));
    if (status == ESP_NOW_SEND_SUCCESS)
    {
        Serial.println("Data sent successfylly");
        previousReceived = true;
    }
    else
    {
        Serial.println("Delivery fail");
    }
}

void initESP_NOW()
{
    if (esp_now_init() != ESP_OK)
    {
        Serial.println("Error initializing ESP-NOW");
        return;
    }
    esp_now_register_send_cb(OnDataSent);

    initPeers();

    addPeer(bridge1);
    addPeer(bridge6);
    addPeer(bridge11);

    Serial.println("ESP-NOW initiated");
}

boolean sendEspNowMessage(float value, const char type[], uint8_t *address)
{
    message.value = value;
    strcpy(message.type, type);

    // max Tries
    int msgSent = 1;

    // Send the message using ESP-NOW
    esp_err_t result = ESP_FAIL;
    while (!previousReceived && msgSent <= MAX_TRIES)
    {
        Serial.printf("Try sending (try %d) msg.id: %d, msg.value: %f, msg.type: %s\n",
                      msgSent++, message.id, message.value, message.type);
        result = esp_now_send(address, (uint8_t *)&message, sizeof(message));
        delay(2000);
        if (result != ESP_OK)
            Serial.println("Error sending ESP-NOW message");
        else
        {
            Serial.println("Sending ESP-NOW message");
        }
    }
    delay(1000);
    if (previousReceived)
    {
        previousReceived = false;
        return true;
    }
    else
    {
        return false;
    }
}

void setup()
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    pinMode(LED_BUILTIN, OUTPUT);

    delay(5000);
    Serial.print("MAC: ");
    Serial.println(WiFi.macAddress());

    WiFi.mode(WIFI_MODE_STA);
    WiFi.disconnect();
    esp_wifi_set_channel(currentPeer->channel, WIFI_SECOND_CHAN_NONE);

    Serial.print("Station IP Address: ");
    Serial.println(WiFi.localIP());
    Serial.print("Wi-Fi Channel: ");
    Serial.println(WiFi.channel());

    initESP_NOW();

    message.id = msgId++;
    strcpy(message.type, "test");
    message.value = 42.;

    // Testing all canals
    // esp_wifi_set_channel(bridge1.channel, WIFI_SECOND_CHAN_NONE);
    // sendEspNowMessage(message.value, message.type, bridge1.address);
    // esp_wifi_set_channel(bridge6.channel, WIFI_SECOND_CHAN_NONE);
    // sendEspNowMessage(message.value, message.type, bridge6.address);
    // esp_wifi_set_channel(bridge11.channel, WIFI_SECOND_CHAN_NONE);
    // sendEspNowMessage(message.value, message.type, bridge11.address);

    int i = 0;
    while (!sendEspNowMessage(message.value, message.type, currentPeer->address) || i < 3)
    {
        currentPeer = currentPeer->next;
        esp_wifi_set_channel(currentPeer->channel, WIFI_SECOND_CHAN_NONE);
        i++;
    }

    Serial.print("Going to sleep for ");
    Serial.print(DEEP_SLEEP_TIME);
    Serial.println(" seconds...");
    esp_sleep_enable_timer_wakeup(DEEP_SLEEP_TIME * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
}

void loop()
{
    // put your main code here, to run repeatedly:
}

#endif