#ifdef BRIDGE

#include <esp_now.h>
#include <esp_wifi.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <LittleFS.h>

const char *password = "ESP32_password";

/* --------------------------------------------- Web -------------------------------------------------- */
AsyncWebServer server(80);

void handleRoot(AsyncWebServerRequest *request)
{
  String html = "<html><body><h1>ESP32 LittleFS File Server</h1>";
  html += "<br><br><table><tr><th>File Name</th><th>File Size</th><th>Download</th><th>Delete</th></tr>";
  File root = LittleFS.open("/");
  File file = root.openNextFile();
  while (file)
  {
    html += "<tr><td>" + String(file.name()) + "</td>";
    html += "<td>" + String(file.size()) + "</td>";
    html += "<td><a href=\"/download?file=" + String(file.name()) + "\">Download</a></td>";
    html += "<td><a href=\"/delete?file=" + String(file.name()) + "\">Delete</a></td></tr>";
    file = root.openNextFile();
  }
  html += "</table></body></html>";
  request->send(200, "text/html", html);
}

void handleDownload(AsyncWebServerRequest *request)
{
  String filename = "/" + request->getParam("file")->value();
  File file = LittleFS.open(filename, "r");
  size_t fileSize = file.size();
  request->send(LittleFS, filename, "application/octet-stream", fileSize);
}

void handleDelete(AsyncWebServerRequest *request)
{
  String filename = "/" + request->getParam("file")->value();
  LittleFS.remove(filename);
  request->redirect("/");
}

/* --------------------------------------------- RSSI -------------------------------------------------- */
int rssi_display;

typedef struct
{
  unsigned frame_ctrl : 16;
  unsigned duration_id : 16;
  uint8_t addr1[6]; /* receiver address */
  uint8_t addr2[6]; /* sender address */
  uint8_t addr3[6]; /* filtering address */
  unsigned sequence_ctrl : 16;
  uint8_t addr4[6]; /* optional */
} wifi_ieee80211_mac_hdr_t;

typedef struct
{
  wifi_ieee80211_mac_hdr_t hdr;
  uint8_t payload[0]; /* network data ended with 4 bytes csum (CRC32) */
} wifi_ieee80211_packet_t;

void promiscuous_rx_cb(void *buf, wifi_promiscuous_pkt_type_t type)
{
  // All espnow traffic uses action frames which are a subtype of the mgmnt frames so filter out everything else.
  if (type != WIFI_PKT_MGMT)
    return;

  const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buf;
  const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
  const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;

  int rssi = ppkt->rx_ctrl.rssi;
  rssi_display = rssi;
}

/* ----------------------------- ESP-NOW ----------------------------- */

typedef struct message
{
  unsigned long id;
  float value;
  char type[16];
} message_t;

message_t message;

esp_now_peer_info_t end_node;

void OnDataRecv(const uint8_t *mac, const uint8_t *data, int len)
{
  // Open log file in append mode

  memcpy(&message, data, sizeof(message));

  File logFile = LittleFS.open(LOG_FILE, "a");

  if (!logFile)
  {
    Serial.println("Failed to open log file");
    return;
  }

  // Write the received data to the log file
  Serial.printf("%02X:%02X:%02X:%02X:%02X:%02X | %d, %s, %f, %d", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5],
                message.id, message.type, message.value, rssi_display);
  logFile.printf("%02X:%02X:%02X:%02X:%02X:%02X | %d, %s, %f, %d\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5],
                 message.id, message.type, message.value, rssi_display);

  // Close the log file
  logFile.close();
}

void initEspNow()
{
  // Initilize ESP-NOW
  if (esp_now_init() != ESP_OK)
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Register callback function
  esp_now_register_recv_cb(OnDataRecv);

  esp_wifi_set_promiscuous(true);
  esp_wifi_set_promiscuous_rx_cb(&promiscuous_rx_cb);
}

/* -------------------------- ARDUINO MAIN ----------------------------- */

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  // Initialize LittleFS
  if (!LittleFS.begin())
  {
    Serial.println("Failed to initialize LittleFS");
    return;
  }

  // Create a log file if it doesn't exist
  if (!LittleFS.exists(LOG_FILE))
  {
    File logFile = LittleFS.open(LOG_FILE, "w");
    if (!logFile)
    {
      Serial.println("Failed to create log file");
      return;
    }
    logFile.close();
  }

  WiFi.mode(WIFI_AP_STA);
  // WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  esp_wifi_set_promiscuous(true);
  if (esp_wifi_set_channel(CHANNEL, WIFI_SECOND_CHAN_NONE) == ESP_OK)
  {
    Serial.println("OK");
  }
  else
  {
    Serial.println("NOT OK");
  }
  // esp_wifi_set_channel(CHANNEL, WIFI_SECOND_CHAN_NONE);
  // esp_wifi_set_promiscuous(false);
  // delay(2000);
  // esp_wifi_set_promiscuous(false);
  WiFi.softAP(MY_SSID, password, CHANNEL, 0, 4);
  // WiFi.begin();
  Serial.println(CHANNEL);
  Serial.println(WiFi.channel());
  Serial.println(WiFi.macAddress());
  Serial.println(WiFi.softAPIP());
  initEspNow();
  // WiFi.softAP(MY_SSID, password);

  delay(1000);

  server.on("/", HTTP_GET, handleRoot);
  server.on("/download", HTTP_GET, handleDownload);
  server.on("/delete", HTTP_GET, handleDelete);
  server.begin();
  Serial.println("HTTP server started");
}

void loop()
{
  // put your main code here, to run repeatedly:
}

#endif