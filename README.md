# Expérience choix de canaux Wi-Fi ESP-NOW

## Description

On cherche à trouver si une solution à des congestions temporaires de canaux Wi-Fi pourraient être résolu par le choix entre les 3 bandes indépendantes : 1, 6 et 11. La stratégie est un de commencer par un des canaux et de le conserver à chaque envoie tant qu'il marche et de sinon basculer sur jusqu'à en trouver un ou les épuiser.

Chaque `bridge` sur chacun des canaux stocke les messages dans un fichier accessible via un point d'accès (ESP32-X) à l'adresse http://192.168.4.1.

## Build

Pour le noeud `end_node` il suffit de taper la commande platformio :

```
pio run -t upload -e end_node
```

Pour les `bridgeX` on peut utiliser le script shell :

```
./build.sh bridgeX
```

avec $X = \{1,6,11\}$.

## Secrets

Il faut ajouter dans [include](include/) un fichier `secrets.h` contenant les addresses mac des 3 noeuds de la forme:

```cpp
const unsigned char ADDRESS_1[] = {0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45};
const unsigned char ADDRESS_6[] = {0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45};
const unsigned char ADDRESS_11[] = {0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45};
```